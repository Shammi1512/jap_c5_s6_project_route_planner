package RoutePlanner;


import java.io.IOException;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;

public class MainClass {
public static void main(String[] args) throws IOException {
	ReadFlightDetails fd=new ReadFlightDetails();
	
	List<Route> routes=fd.readRecords("route1.csv");
	
	
	
	
	System.out.println(" \n \n List of available flights are  \n \n");
	System.out.println(" \n _____________________________________________________ \n");
	
	System.out.println(String.format("%-15s%-15s%-15s%-15s%-15s", "Source", "Destination", "Distance","Time","Fare"));
	System.out.println();
	for(Route r: routes)
	{
		 System.out.println(String.format("%-15s%-15s%-15s%-15s%-15s", r.getFrom(), r.getTo(), r.getDistance(),r.getTime(),r.getFare()));
		//System.out.println(lir.next());
	}
	System.out.println(" \n _____________________________________________________ \n");
	
	
	Scanner sc=new Scanner(System.in);
	System.out.print("Enter the name of source city : ");
	String fromCity=sc.nextLine();
	//System.out.println(" \n _____________________________________________________ \n");
	fd.showDirectFlights(routes, fromCity);
	fd.sortDirectFlights(routes);
//	
//	
//	
	System.out.println(" \n _____________________________________________________ \n");
	System.out.print("Enter the source city ");
	String sourceCity=sc.nextLine();
	System.out.println();
	System.out.print("enter the destination city  ");
	String desCity=sc.nextLine();
	//System.out.println(" \n _____________________________________________________ \n");
	//String desCity=sc.nextLine();
	fd.showDirectSourceToDesti(routes,sourceCity,desCity);
	fd.showAllConnections(routes,sourceCity,desCity);
}
}
